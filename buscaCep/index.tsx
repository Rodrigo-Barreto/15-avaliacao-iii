const express = require('express')
const app = express();
const axios = require("axios")

var convert = require("xml-js");






app.use('/:estado/:cidade/:logradouro/', async (req:any, res:any, next:any) => {
    await axios.get('https://viacep.com.br/ws/' + req.params.estado + '/' + req.params.cidade + '/' + req.params.logradouro + '/json/')
        .then((response:any) => res.send(response.data))
        .catch((err) => res.send('Ocorreu um erro'))
    next()
})
app.use('/:estado/:cidade/:logradouro/xml', async (req:any, res:any, next:any) => {
    await axios.get('https://viacep.com.br/ws/' + req.params.estado + '/' + req.params.cidade + '/' + req.params.logradouro + '/json/')
        .then((response:any) => {

            let json = require(' fs ').readFileSync(response.data, ' UTF-8 ');
            let options = { compact: true, ignoreComment: true, spaces: 4 };
            let result = convert.json2xml(json, options);
            res.send(result)

        })
        .catch((err:any) => res.send('Ocorreu um erro'))
    next()
})


app.listen(5000, function (erro:any) {
    if (erro) {
        console.log('algo deu errado')
    } else {
        console.log('servidor iniciado com sucesso')


    }
})
